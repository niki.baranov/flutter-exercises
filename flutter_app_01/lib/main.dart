import 'package:flutter/material.dart';

import './question.dart';
import './answer.dart';
import './quiz.dart';
import './result.dart';

//void main (){
//  runApp(MyApp());
//}

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState

    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final _questions = const [
    {
      'questionText': 'Pick a color',
      'answers': [
        {'text': 'Blue', 'score': 6},
        {'text': 'Black', 'score': 1},
        {'text': 'White', 'score': 10},
        {'text': 'Pink', 'score': 4},
      ],
    },
    {
      'questionText': 'Pick an animal',
      'answers': [
        {'text': 'Wolf', 'score': 1},
        {'text': 'Cat', 'score': 3},
        {'text': 'Dog', 'score': 10},
        {'text': 'Snake', 'score': 8}
      ],
    },
    {
      'questionText': 'Favorite car brand',
      'answers': [
        {'text': 'VW', 'score': 10},
        {'text': 'Audi', 'score': 7},
        {'text': 'Ford', 'score': 4},
        {'text': 'Tesla', 'score': 1}
      ],
    },
  ];
  var _totalScore = 0;
  var _questionIndex = 0;

  void _resetQuiz() {
    setState(() {
      _totalScore = 0;
      _questionIndex = 0;
    });
  }

  void _answerQuestion(int score) {
    _totalScore += score;

    setState(() {
      _questionIndex = _questionIndex + 1;
    });
    print('Answer selected');
    print(_questionIndex);

    if (_questionIndex < _questions.length) {
      print('More questions!');
    } else {
      print('No questions left');
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Yodo App'),
        ),
        body: _questionIndex < _questions.length
            ? Quiz(
                answerQuestion: _answerQuestion,
                questionIndex: _questionIndex,
                questions: _questions,
              )
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }
}
